package task1.tests;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

/**
 * Created by AleksandrKos on 13.09.2015.
 */
public class TestBaseTut {

    private final Logger TUT_BY_LOGGER = Logger.getLogger(TestBaseTut.class);
    protected WebDriver driver;

    //The method initializes the driver and moves to the home page. Check the address of the page after the jump
    @BeforeTest
    @Parameters("URL_NAME")
    public void setUp(String URL_NAME) {
        driver = new FirefoxDriver();
        TUT_BY_LOGGER.info("Connecting driver. Driver name = " + driver.getClass().getSimpleName());
        driver.get(URL_NAME);
        TUT_BY_LOGGER.info("Go to page " + URL_NAME);
        if (!driver.getCurrentUrl().contains(URL_NAME)) {
            TUT_BY_LOGGER.warn("IllegalStateException.");
            TUT_BY_LOGGER.info("This is not the page you are expected.");
            throw new IllegalStateException();
        }
    }

    //The finishing method which closes the driver.
    @AfterTest
    public void tearDown() {
        driver.close();
        TUT_BY_LOGGER.info("Test is over.");
    }

}
