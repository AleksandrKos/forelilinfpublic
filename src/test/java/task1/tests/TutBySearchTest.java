package task1.tests;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriverException;
import org.testng.annotations.*;
import task1.pages.HomeTutBy;
import task1.pages.ResultPage;

/**
 * Created by AleksandrKos on 06.09.2015.
 */
/*This test looks on the page String(SEARCH_MAIN_TEXT) and counts the number of results.
 Then, in the results he was looking for the second String(SEARCH_TEXT)
String value set through the configuration file.*/
public class TutBySearchTest extends TestBaseTut {

    private final Logger TUT_BY_LOGGER = Logger.getLogger(TutBySearchTest.class);

    @Test
    @Parameters({"SEARCH_MAIN_TEXT", "SEARCH_TEXT"})
    public void testSearch(String SEARCH_MAIN_TEXT, String SEARCH_TEXT) {
        HomeTutBy home = new HomeTutBy(driver);
        TUT_BY_LOGGER.info("Home page obtained.");
        //Search the first String
        ResultPage result = home.search(SEARCH_MAIN_TEXT);
        TUT_BY_LOGGER.info("Search request = " + SEARCH_MAIN_TEXT);
        //Counting the results and conclusions in the log
        TUT_BY_LOGGER.info(result.calculate() + " results were found on the page.");
        //Search the second String
        try {
            result.searchTextInResultList(SEARCH_TEXT);
            TUT_BY_LOGGER.info(SEARCH_TEXT + " was found in the results.");
        } catch (WebDriverException ex) {
            TUT_BY_LOGGER.warn("WebDriverException: " + ex);
            TUT_BY_LOGGER.info(SEARCH_TEXT + " wasn't found in the results.");
        }
    }
}


