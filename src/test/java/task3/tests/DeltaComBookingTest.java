package task3.tests;

import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;
import task3.beans.CombinationBean;
import task3.pages.*;
import task3.services.ParserXMLforDelta;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by AleksandrKos on 11.09.2015.
 */
public class DeltaComBookingTest extends TestBaseDelta {

    private final Logger DELTA_LOGGER = Logger.getLogger(DeltaComBookingTest.class);

    //The method uses a DataProvider to get two beans
    @Test(dataProvider = "FlightCombinationProvider")
    public void BookingTest(CombinationBean combinations) {
        HomeDelta home = new HomeDelta(driver);
        //Filling the search form of tickets and pressing "FIND FLIGHTS"
        TicketsSelectionPage selectionPage = home.fillBookingWidget(combinations.getFlightCombinationBean());
        DELTA_LOGGER.info("Search form ticket is filled with values from the file.");
        //Selecting tickets and confirmation
        TripSummaryPage tripSummary = selectionPage.selectTickets();
        DELTA_LOGGER.info("Select both tickets.");
        //Next page
        PassengerInfoPage passengerInfo = tripSummary.goNext(combinations.getPersonalInformationBean());
        DELTA_LOGGER.info("Confirmation and next page.");
        //Filling the form of passenger info and confirmation
        CreditCardInfoPage creditCardInfo = passengerInfo.fillAllFields();
        DELTA_LOGGER.info("The form with passenger info is filled.");
        creditCardInfo.check();
        DELTA_LOGGER.info("Open the page with credit card information.");
    }

    //DataProvider gives XML file "flightCombinationForDeltaTest.xml" to ParserXMLforDelta
    @DataProvider(name = "FlightCombinationProvider")
    private Iterator<CombinationBean[]> createFlightCovbinationBean(Method m) {
        List<CombinationBean[]> combinations = new ArrayList<CombinationBean[]>();
        File file = new File(System.getProperty("user.dir") + "/src/test/resources/flightCombinationForDeltaTest.xml");
        try {
            combinations = ParserXMLforDelta.ParseXMLandCreateBean(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        return combinations.iterator();
    }
}
