package task3.tests;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

/**
 * Created by AleksandrKos on 11.09.2015.
 */
public class TestBaseDelta {

    private final Logger DELTA_LOGGER = Logger.getLogger(TestBaseDelta.class);
    protected WebDriver driver;

    //The method initializes the driver and moves to the home page. Check the address of the page after the jump
    @BeforeTest
    @Parameters({"URL_NAME"})
    public void setUp(String URL_NAME) {
        driver = new FirefoxDriver();
        DELTA_LOGGER.info("Connecting driver. Driver name = " + driver.getClass().getSimpleName());
        driver.get(URL_NAME);
        if (!driver.getCurrentUrl().contains(URL_NAME)) {
            DELTA_LOGGER.warn("IllegalStateException.");
            DELTA_LOGGER.info("This is not the page you are expected.");
            throw new IllegalStateException();
        }
        DELTA_LOGGER.info("Go to page " + URL_NAME);

    }

    //The finishing method which closes the driver
    @AfterTest
    public void tearDown() {
        driver.close();
        DELTA_LOGGER.info("Test is over.");
    }
}
