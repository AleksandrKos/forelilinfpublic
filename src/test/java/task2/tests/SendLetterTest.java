package task2.tests;

import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;
import task2.beans.LetterBean;
import task2.pages.HomeGoogle;
import task2.pages.LetterGoogle;
import task2.pages.LoginGoogle;
import task2.pages.MailGoogle;
import task2.services.ParserXML;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by AleksandrKos on 10.09.2015.
 */

//This test comes into google account and send the letters. The amount of letters is determined in DataProvider
public class SendLetterTest extends TestBaseGoogle {

    private final Logger GOOGLE_LOGGER = Logger.getLogger(SendLetterTest.class);

    @Test(dataProvider = "LetterProvider")
    public void sendNewLetter(LetterBean letterBean) {
        HomeGoogle home = new HomeGoogle(driver);

        //Only go to google mail
        LoginGoogle loginPage = home.goToMail();

        //The method fills the login field, sends; fills the password field, sends
        MailGoogle mail = loginPage.login(USER_LOGIN, PASSWORD);
        StringBuffer sb = new StringBuffer();
        GOOGLE_LOGGER.info(sb.append("Login with login: ").append(USER_LOGIN).append(" and password: ")
                .append(PASSWORD).append(" is successful."));
        //Go to send messages form
        LetterGoogle letter = new LetterGoogle(driver);
        letter.sendNewLetter(letterBean.getEmail(), letterBean.getTheme(), letterBean.getMessage());
        mail.logout();
    }
// DataProvider reads the file "lettersForGoogleTest.xml" and creates beans array
    @DataProvider(name = "LetterProvider")
    private static Iterator<LetterBean[]> createLetter(Method m){
        List<LetterBean[]> letters = new ArrayList<LetterBean[]>();
        File file = new File(System.getProperty("user.dir")+"/src/test/resources/lettersForGoogleTest.xml");
        try {
            letters = ParserXML.ParseXMLandCreateBean(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        return letters.iterator();
    }
}
