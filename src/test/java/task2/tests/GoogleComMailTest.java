package task2.tests;

import org.apache.log4j.Logger;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import task2.pages.HomeGoogle;
import task2.pages.LoginGoogle;
import task2.pages.MailGoogle;

/**
 * Created by AleksandrKos on 06.09.2015.
 */
public class GoogleComMailTest extends TestBaseGoogle {

    private final Logger GOOGLE_LOGGER = Logger.getLogger(GoogleComMailTest.class);

    @Test
    @Parameters({"WORD_FOR_SEARCH_EMAIL", "EXPECTED_AMOUNT"})
    public void googleMailTest(String WORD_FOR_SEARCH_EMAIL, int EXPECTED_AMOUNT) {
        HomeGoogle home = new HomeGoogle(driver);

        //Only go to google mail
        LoginGoogle loginPage = home.goToMail();

        //The method fills the login field, sends; fills the password field, sends
        MailGoogle mail = loginPage.login(USER_LOGIN, PASSWORD);
        StringBuffer sb = new StringBuffer();
        GOOGLE_LOGGER.info(sb.append("Login with login: ").append(USER_LOGIN).append(" and password: ")
                .append(PASSWORD).append(" is successful."));
        //Execute checks on the mail page
        GOOGLE_LOGGER.info("Open incoming messages view = " + mail.isIncomingMessageView());
        GOOGLE_LOGGER.info("Looking messages in all folders. Word for search = " + WORD_FOR_SEARCH_EMAIL);
        GOOGLE_LOGGER.info("Expected amount of results = " + EXPECTED_AMOUNT);
        sb = new StringBuffer();
        GOOGLE_LOGGER.info(sb.append("There are ").append(mail.howManyMessages(WORD_FOR_SEARCH_EMAIL)).append(" messages"));
        GOOGLE_LOGGER.info("Can I get outgoing messages? = " + mail.isOutgoingMessage());
        GOOGLE_LOGGER.info("Can I get spam? = " + mail.isSpam());
        mail.logout();
        GOOGLE_LOGGER.info("Logout");
    }
}

