package task2.tests;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.*;

/**
 * Created by AleksandrKos on 10.09.2015.
 */
public class TestBaseGoogle {

    private final Logger GOOGLE_LOGGER = Logger.getLogger(TestBaseGoogle.class);
    protected WebDriver driver;
    protected static String USER_LOGIN;
    protected static String PASSWORD;

    //The method initializes the driver and moves to the home page. Check the address of the page after the jump
    @BeforeMethod
    @Parameters({"URL_NAME", "USER_LOGIN", "PASSWORD"})
    public void setUp(String URL_NAME, String USER_LOGIN, String PASSWORD) {
        this.USER_LOGIN = USER_LOGIN;
        this.PASSWORD = PASSWORD;
        driver = new FirefoxDriver();
        GOOGLE_LOGGER.info("Connecting driver. Driver name = " + driver.getClass().getSimpleName());
        driver.get(URL_NAME);
        if (!driver.getCurrentUrl().contains(URL_NAME)) {
            GOOGLE_LOGGER.warn("IllegalStateException.");
            GOOGLE_LOGGER.info("This is not the page you are expected.");
            throw new IllegalStateException();
        }
        GOOGLE_LOGGER.info("Go to page " + URL_NAME);
    }

    //The finishing method which closes the driver.
    @AfterMethod
    public void tearDown() {
        driver.close();
        GOOGLE_LOGGER.info("Test is over.");
    }
}
