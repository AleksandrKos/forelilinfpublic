package task1.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by AleksandrKos on 06.09.2015.
 */
//Class for Home page
public class HomeTutBy {
    private WebDriver driver;

    @FindBy(id = "search_from_str")
    private WebElement searchField;

    @FindBy(name = "search")
    private WebElement searchButton;

    public HomeTutBy(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    //Method enters a search query and moves to the ResultPage
    public ResultPage search(String text) {
        searchField.sendKeys(text);
        searchButton.click();
        return new ResultPage(driver);
    }
}
