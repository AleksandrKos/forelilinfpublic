package task1.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import java.util.List;

/**
 * Created by AleksandrKos on 06.09.2015.
 */
//Class for ResultPage
public class ResultPage {

    private WebDriver driver;
    private final By SEARCH_RESULT_ELEMENTS = By.className("b-results__li");


    public ResultPage(WebDriver driver) {
        this.driver = driver;
    }

    //Method defines the amount of elements in the list of results
    public int calculate() {
        return getSearchResultList().size();
    }

    //Method searches String(text) in the list of results. If element isn't int list throws Exception
    public void searchTextInResultList(String text) throws WebDriverException {
        List<WebElement> resultList = getSearchResultList();
        boolean marker = false;
        for (int i = 0; i < resultList.size(); i++) {
            WebElement webElement = resultList.get(i);
            if (webElement.getText().contains(text)) {
                webElement.click();
                marker = true;
            }
        }
        if (!marker) {
            throw new WebDriverException();
        }
    }

    //Method returns the list of results
    private List<WebElement> getSearchResultList() {
        return driver.findElements(SEARCH_RESULT_ELEMENTS);
    }
}
