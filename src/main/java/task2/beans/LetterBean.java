package task2.beans;

/**
 * Created by AleksandrKos on 10.09.2015.
 */
public class LetterBean {
    private String email;
    private String theme;
    private String message;

    public LetterBean() {
    }

    public String getEmail() {
        return email;
    }

    public String getTheme() {
        return theme;
    }

    public String getMessage() {
        return message;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
