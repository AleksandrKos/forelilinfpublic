package task2.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.concurrent.TimeUnit;

/**
 * Created by AleksandrKos on 14.09.2015.
 */
public class LoginGoogle {

    private WebDriver driver;

    @FindBy(id = "Email")
    private WebElement emailField;

    @FindBy(id = "next")
    private WebElement buttonNext;

    @FindBy(id = "Passwd")
    private WebElement passwordField;

    @FindBy(id = "signIn")
    private WebElement buttonSegnIn;

    @FindBy(className = "hidden-small")
    private WebElement textOnWelcomPage;

    public LoginGoogle(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    //Method fills the fields "login" and "password"
    public MailGoogle login(String login, String password) {
        emailField.sendKeys(login);
        buttonNext.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        passwordField.sendKeys(password);
        buttonSegnIn.click();
        return new MailGoogle(driver);
    }
}
