package task2.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Created by AleksandrKos on 10.09.2015.
 */
public class LetterGoogle {

    private final By LETTERS_BODY = By.className("editable");
    private final By SUBMIT_BUTTON = By.className("T-I-atl");
    private final By LINK_SEE_LETTERS = By.id("link_vsm");
    private WebDriver driver;

    @FindBy(xpath = "//div[@id=':4i']/div/div")
    private WebElement buttonNewLetter;

    @FindBy(name = "to")
    private WebElement fieldTo;

    @FindBy(name = "subjectbox")
    private WebElement fieldTheme;

    public LetterGoogle(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    //The method fills fields email, theme, text and send email
    public void sendNewLetter(String email, String theme, String text) {
        buttonNewLetter.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        fieldTo.sendKeys(email);
        fieldTheme.sendKeys(theme);
        //Find field text area
        driver.findElement(By.className("aoX")).findElement(LETTERS_BODY).sendKeys(text);
        //Find button submit
        driver.findElement(By.className("IZ")).findElement(SUBMIT_BUTTON).click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(LINK_SEE_LETTERS));
        driver.findElement(LINK_SEE_LETTERS);
    }

}
