package task2.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by AleksandrKos on 06.09.2015.
 */
public class HomeGoogle {

    private WebDriver driver;

    @FindBy(className = "gb_P")
    private WebElement linkGoToMailPage;

    public HomeGoogle(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    // Just go to MailGoogle
    public LoginGoogle goToMail() {
        linkGoToMailPage.click();
        return new LoginGoogle(driver);
    }

}
