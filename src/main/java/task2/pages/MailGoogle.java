package task2.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by AleksandrKos on 08.09.2015.
 */
public class MailGoogle {

    private WebDriver driver;

    @FindBy(className = "gbqfif")
    private WebElement mainSearchLine;

    @FindBy(css = "button#gbqfb")
    private WebElement buttonSearch;

    @FindBy(xpath = ".//*[@id=':4u']/div/div[1]/span/a")
    private WebElement buttonOutgoingMessage;

    @FindBy(className = "CJ")
    private WebElement buttonOpenListMessageCategories;

    @FindBy(xpath = ".//*[@id=':4x']/div/div[1]/span/a")
    private WebElement buttonSpam;

    @FindBy(css = "a[title^=Аккаунт]")
    private WebElement accaountName;

    @FindBy(id = "gb_71")
    private WebElement buttonExite;

    private final By EXPECTED_ELEMENT_FOR_SPAM = By.className("ya");
    private final String EXPECTED_TEXT_FOR_OUTGOING_MESSAGE = "Кому";
    private final By BOOKMARK_WITH_EXPECTED_TEXT = By.id(":2w");
    private final By ONE_ROW_OUTGOING_MESSAGE = By.className("yW");
    private final By ONE_ROW_OF_THE_FOUND_LIST = By.className("zA");
    private final By CELL_WITH_RECIPIENT = By.cssSelector("span.zF");

    MailGoogle(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public boolean isIncomingMessageView() {
        try {
            driver.findElement(BOOKMARK_WITH_EXPECTED_TEXT);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    //The method moves to the page of outgoing emails, and searches for a word "Кому" in all rows
    public boolean isOutgoingMessage() {
        buttonOutgoingMessage.click();
        for (int i = 0; i < driver.findElements(ONE_ROW_OUTGOING_MESSAGE).size(); i++) {
            if (driver.findElements(ONE_ROW_OUTGOING_MESSAGE).get(i).getText().contains(EXPECTED_TEXT_FOR_OUTGOING_MESSAGE)) {
                return true;
            }
        }
        return false;
    }

    //The method moves to the page of spam, and searches for a string "в спаме" in the element above the list of messages
    public boolean isSpam() {
        buttonOpenListMessageCategories.click();
        buttonSpam.click();
        try {
            driver.findElement(EXPECTED_ELEMENT_FOR_SPAM);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    //Method input in the search line query(wordForSearch) and finds amount of results.
    //I noticed that there are 3 blank lines.
    public int howManyMessages(String wordForSearch) {
        mainSearchLine.clear();
        mainSearchLine.sendKeys(wordForSearch);
        buttonSearch.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        createList();
        return (driver.findElements(ONE_ROW_OF_THE_FOUND_LIST).size() - 3);
    }

    //Logout: open account menu and click button "Выйти"
    public void logout() {
        accaountName.click();
        buttonExite.click();
    }

    //The method makes the list, which are the recipients of letters found
    //Here, I take only one value - the addressee.
    private List<List<String>> createList() {
        List<WebElement> trList = driver.findElements(ONE_ROW_OF_THE_FOUND_LIST);
        List<List<String>> letterList = new ArrayList<List<String>>();
        List<String> temp = new ArrayList<String>();
        for (int i = 3; i < trList.size(); i++) {
//            System.out.println(trList.get(i).findElement(By.cssSelector("span.zF")).getAttribute("name"));
            temp.add(trList.get(i).findElement(CELL_WITH_RECIPIENT).getAttribute("name"));
            letterList.add(temp);
        }
        return letterList;
    }
}
