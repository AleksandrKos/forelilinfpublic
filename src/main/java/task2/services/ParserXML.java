package task2.services;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import task2.beans.LetterBean;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AleksandrKos on 24.09.2015.
 */
public class ParserXML {
    public static List<LetterBean[]> ParseXMLandCreateBean(File fileXML) throws ParserConfigurationException,
            IOException, SAXException {
        List<LetterBean[]> letters = new ArrayList<LetterBean[]>();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(fileXML);
        //Find all tags <letter> and get attributes "email", "theme" and "message" for create LetterBean
        NodeList list = doc.getElementsByTagName("letter");
        for (int i = 0; i < list.getLength(); i++) {
            LetterBean bean = new LetterBean();
            Node childNode = list.item(i);
            NamedNodeMap attributes = childNode.getAttributes();
            Node email = attributes.getNamedItem("email");
            bean.setEmail(email.getNodeValue());

            Node theme = attributes.getNamedItem("theme");
            bean.setTheme(theme.getNodeValue());

            Node message = attributes.getNamedItem("message");
            bean.setMessage(message.getNodeValue());

            letters.add(new LetterBean[]{bean});
        }
        return letters;
    }
}
