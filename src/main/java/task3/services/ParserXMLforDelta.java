package task3.services;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import task3.beans.CombinationBean;
import task3.beans.FlightCombinationBean;
import task3.beans.PersonalInformationBean;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AleksandrKos on 25.09.2015.
 */
public class ParserXMLforDelta {

    //Parser reads the file and create two beans
    public static List<CombinationBean[]> ParseXMLandCreateBean(File fileXML) throws ParserConfigurationException,
            IOException, SAXException {

        List<CombinationBean[]> combinations = new ArrayList<CombinationBean[]>();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(fileXML);
        NodeList listFlightCombinations = doc.getElementsByTagName("searchForm");
        NodeList listPersonalInfo = doc.getElementsByTagName("passengerInfo");
        for (int i = 0; i < listFlightCombinations.getLength(); i++) {

            //Create FlightCombinationBean
            FlightCombinationBean beanFlightCombinations = new FlightCombinationBean();
            Node childNodeFlightCovbination = listFlightCombinations.item(i);
            NamedNodeMap attrFlightCombination = childNodeFlightCovbination.getAttributes();

            Node tripType = attrFlightCombination.getNamedItem("tripType");
            beanFlightCombinations.setTripType(tripType.getNodeValue());

            Node from = attrFlightCombination.getNamedItem("from");
            beanFlightCombinations.setFrom(from.getNodeValue());

            Node to = attrFlightCombination.getNamedItem("to");
            beanFlightCombinations.setTo(to.getNodeValue());

            Node dateType = attrFlightCombination.getNamedItem("dateType");
            beanFlightCombinations.setDateType(dateType.getNodeValue());

            Node priceIn = attrFlightCombination.getNamedItem("priceIn");
            beanFlightCombinations.setPriceIn(priceIn.getNodeValue());

            //Create PersonalInformationBean
            PersonalInformationBean beanPersonalInfo = new PersonalInformationBean();
            Node childNodePersonalInfo = listPersonalInfo.item(i);
            NamedNodeMap attrPersonalInfo = childNodePersonalInfo.getAttributes();

            Node prefix = attrPersonalInfo.getNamedItem("prefix");
            beanPersonalInfo.setPrefix(prefix.getNodeValue());

            Node name = attrPersonalInfo.getNamedItem("name");
            beanPersonalInfo.setFirstName(name.getNodeValue());

            Node surname = attrPersonalInfo.getNamedItem("surname");
            beanPersonalInfo.setLastName(surname.getNodeValue());

            Node gender = attrPersonalInfo.getNamedItem("gender");
            beanPersonalInfo.setGender(gender.getNodeValue());

            Node month = attrPersonalInfo.getNamedItem("month");
            beanPersonalInfo.setMonth(month.getNodeValue());

            Node day = attrPersonalInfo.getNamedItem("day");
            beanPersonalInfo.setDay(day.getNodeValue());

            Node year = attrPersonalInfo.getNamedItem("year");
            beanPersonalInfo.setYear(year.getNodeValue());

            Node deviceType = attrPersonalInfo.getNamedItem("deviceType");
            beanPersonalInfo.setDeviceType(deviceType.getNodeValue());

            Node countryCode = attrPersonalInfo.getNamedItem("countryCode");
            beanPersonalInfo.setCountryCode(countryCode.getNodeValue());

            Node telephoneNumber = attrPersonalInfo.getNamedItem("telephoneNumber");
            beanPersonalInfo.setPhoneNumber(telephoneNumber.getNodeValue());

            Node email = attrPersonalInfo.getNamedItem("email");
            beanPersonalInfo.setEmail(email.getNodeValue());

            //Create CombinationBean and add his to List
            CombinationBean combinationBean = new CombinationBean(beanFlightCombinations, beanPersonalInfo);
            combinations.add(new CombinationBean[]{combinationBean});
        }
        return combinations;
    }
}
