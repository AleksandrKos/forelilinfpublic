package task3.services;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;

/**
 * Created by AleksandrKos on 24.09.2015.
 */
public class DateForBookingWidget {

    private static LocalDate departureDate;

    //Method returns next Friday, as a string
    public static String getDepartureDate(){
        departureDate = LocalDate.now();
        departureDate = departureDate.with(TemporalAdjusters.next(DayOfWeek.FRIDAY));
        departureDate = departureDate.plusWeeks(1);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");
        return departureDate.format(formatter);
    }

    //Method returns Friday in two weeks as a string
    public static String getReturnDate(){
        departureDate = departureDate.plusWeeks(1);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");
        return departureDate.format(formatter);
    }

}
