package task3.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import task3.beans.FlightCombinationBean;
import org.openqa.selenium.WebDriver;
import task3.services.DateForBookingWidget;

/**
 * Created by AleksandrKos on 11.09.2015.
 */
public class HomeDelta extends AbstractPage {

    private WebDriver driver;

    @FindBy(id = "roundTripBtn")
    private WebElement roundTripButton;

    @FindBy(id = "originCity")
    private WebElement fieldFrom;

    @FindBy(id = "destinationCity")
    private WebElement fieldTo;

    @FindBy(id = "departureDate")
    private WebElement fieldDepartureDate;

    @FindBy(id = "returnDate")
    private WebElement fieldReturnDate;

    @FindBy(id = "exactDaysBtn")
    private WebElement exactDaysButton;

    @FindBy(id = "cashBtn")
    private WebElement cashButton;

    @FindBy(id = "findFlightsSubmit")
    private WebElement findFlightsSubmitButton;

    private final String EXPECTED_TRIP_TYPE = "ROUND TRIP";
    private final String EXPECTED_DATE_TYPE = "EXACT DATES";
    private final String EXPECTED_PRICE_TYPE = "MONEY";

    public HomeDelta(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    //Filling the search form of tickets and pressing "FIND FLIGHTS"
    public TicketsSelectionPage fillBookingWidget(FlightCombinationBean flightCombination) throws RuntimeException {
        waitForLoadingAllElements(driver);
        if (EXPECTED_TRIP_TYPE.equals(flightCombination.getTripType())) {
            roundTripButton.click();
        } else {
            throw new RuntimeException("This test is waiting ROUND TRIP type!");
        }
        fieldFrom.sendKeys(flightCombination.getFrom());
        fieldTo.sendKeys(flightCombination.getTo());

        //Static methods of class DateForBookingWidget set  DepartureDate and ReturnDate
        fieldDepartureDate.sendKeys(DateForBookingWidget.getDepartureDate());
        fieldReturnDate.sendKeys(DateForBookingWidget.getReturnDate());

        if (EXPECTED_DATE_TYPE.equals(flightCombination.getDateType())) {
            exactDaysButton.click();
        } else {
            throw new RuntimeException("This test is waiting EXACT DAYS type!");
        }
        if (EXPECTED_PRICE_TYPE.equals(flightCombination.getPriceIn())) {
            cashButton.click();
        } else {
            throw new RuntimeException("This test is waiting Show Price in MONEY!");
        }
        findFlightsSubmitButton.click();
        waitForLoadingAllElements(driver);
        return new TicketsSelectionPage(driver);
    }
}
