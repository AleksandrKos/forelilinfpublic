package task3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Created by AleksandrKos on 11.09.2015.
 */
public class AbstractPage {

    protected final int WAITING_TIME = 30;

    //The method waits until all the elements are loaded.
    protected void waitForLoadingAllElements(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(WAITING_TIME - 20, TimeUnit.SECONDS);
    }

    //The method waits until the element to be clickable.
    protected void waitUntil(WebDriver driver, By element) {
        WebDriverWait wait = new WebDriverWait(driver, WAITING_TIME);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }
}
