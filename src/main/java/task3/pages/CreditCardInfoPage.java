package task3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by AleksandrKos on 11.09.2015.
 */
public class CreditCardInfoPage extends AbstractPage {

    private final By CONTINUE_BUTTON = By.id("continue_button");
    private WebDriver driver;

    public CreditCardInfoPage(WebDriver driver) {
        this.driver = driver;
    }

    //The method checks for buttons
    public boolean check() {
        try {
            waitForLoadingAllElements(driver);
            driver.findElement(CONTINUE_BUTTON).click();
        } catch (Exception ex) {
            throw new RuntimeException("The button is not found!");
        }
        return true;
    }
}
