package task3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by AleksandrKos on 11.09.2015.
 */
public class TicketsSelectionPage extends AbstractPage {

    private final By PANEL_WITH_ALL_TICKETS = By.id("_fareDisplayContainer_tmplHolder");
    private final By BUTTON_SELECT_FIRST_TICKET = By.id("0_0_0");
    private WebDriver driver;

    public TicketsSelectionPage(WebDriver driver) {
        this.driver = driver;
    }

    //Method selects the first ticket and clicks the select button and then as the first ticket back.
    public TripSummaryPage selectTickets() {
        waitForLoadingAllElements(driver);
        driver.findElement(PANEL_WITH_ALL_TICKETS).findElement(BUTTON_SELECT_FIRST_TICKET).click();
        //Waiting for a page refresh
        waitUntil(driver, BUTTON_SELECT_FIRST_TICKET);
        driver.findElement(BUTTON_SELECT_FIRST_TICKET).click();
        waitForLoadingAllElements(driver);

        return new TripSummaryPage(driver);
    }
}

