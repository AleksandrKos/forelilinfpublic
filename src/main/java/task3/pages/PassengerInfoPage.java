package task3.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import task3.beans.PersonalInformationBean;

/**
 * Created by AleksandrKos on 11.09.2015.
 */
public class PassengerInfoPage extends AbstractPage{
    private WebDriver driver;
    PersonalInformationBean bean;

    @FindBy(id = "prefix0")
    private WebElement selectPrefixElement;

    @FindBy(id = "firstName0")
    private WebElement fieldFirstName;

    @FindBy(id = "lastName0")
    private WebElement fieldLastName;

    @FindBy(id = "gender0")
    private WebElement selectGenderElement;

    @FindBy(id = "month0")
    private WebElement selectMonthElement;

    @FindBy(id = "day0")
    private WebElement selectDayElement;

    @FindBy(id = "year0")
    private WebElement selectYearElement;

    @FindBy(xpath = ".//*[@id='emergencyContact0']/div/div[3]/div[1]/div/fieldset/label[2]/span")
    private WebElement EmergencyContactInformationButtonNo;

    @FindBy(id = "deviceType")
    private WebElement selectDeviceTypeElement;

    @FindBy(id = "countryCode0")
    private WebElement selectCountryCodeElement;

    @FindBy(id = "telephoneNumber0")
    private WebElement fieldTelephoneNumber;

    @FindBy(id = "email")
    private WebElement FieldEmail;

    @FindBy(id = "reEmail")
    private WebElement fieldRepeatEmail;

    @FindBy(id = "paxReviewPurchaseBtn")
    private WebElement confirmButton;

    public PassengerInfoPage(WebDriver driver, PersonalInformationBean personalInformation) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.bean = personalInformation;
    }

    //Filling the form of passenger info and confirmation
    public CreditCardInfoPage fillAllFields() {
        Select selectPrefix = new Select(selectPrefixElement);
        selectPrefix.selectByValue(bean.getPrefix());
        fieldFirstName.sendKeys(bean.getFirstName());
        fieldLastName.sendKeys(bean.getLastName());
        Select selectGender = new Select(selectGenderElement);
        selectGender.selectByValue(bean.getGender());
        Select selectMonth = new Select(selectMonthElement);
        selectMonth.selectByValue(bean.getMonth());
        Select selectDay = new Select(selectDayElement);
        selectDay.selectByValue(bean.getDay());
        Select selectYear = new Select(selectYearElement);
        selectYear.selectByValue(bean.getYear());
        EmergencyContactInformationButtonNo.click();
        Select deviceType = new Select(selectDeviceTypeElement);
        deviceType.selectByValue(bean.getDeviceType());
        Select countryCode = new Select(selectCountryCodeElement);
        countryCode.selectByValue(bean.getCountryCode());
        fieldTelephoneNumber.sendKeys(bean.getPhoneNumber());
        FieldEmail.sendKeys(bean.getEmail());
        fieldRepeatEmail.sendKeys(bean.getEmail());
        confirmButton.click();
        waitForLoadingAllElements(driver);
        return new CreditCardInfoPage(driver);
    }
}
