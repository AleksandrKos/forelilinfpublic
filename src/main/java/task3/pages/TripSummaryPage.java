package task3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import task3.beans.PersonalInformationBean;

/**
 * Created by AleksandrKos on 11.09.2015.
 */


public class TripSummaryPage extends AbstractPage{

    private final By CONTINUE_BUTTON_PANEL = By.id("tripContinueButtonWrap");
    private final By CONTINUE_BUTTON = By.tagName("button");
    private WebDriver driver;

    public TripSummaryPage(WebDriver driver) {
        this.driver = driver;
    }

    //Loading page and jump to next page (click button "Continue")
    public PassengerInfoPage goNext(PersonalInformationBean personalInformation) {
        waitUntil(driver, CONTINUE_BUTTON_PANEL);
        driver.findElement(CONTINUE_BUTTON_PANEL).findElement(CONTINUE_BUTTON).click();
        return new PassengerInfoPage(driver, personalInformation);
    }
}
