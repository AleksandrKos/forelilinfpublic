package task3.beans;

/**
 * Created by AleksandrKos on 26.09.2015.
 */
public class CombinationBean {
    private FlightCombinationBean flightCombinationBean;
    private PersonalInformationBean personalInformationBean;

    public CombinationBean(FlightCombinationBean flightCombinationBean, PersonalInformationBean personalInformationBean){
        this.flightCombinationBean = flightCombinationBean;
        this.personalInformationBean = personalInformationBean;
    }

    public FlightCombinationBean getFlightCombinationBean() {
        return flightCombinationBean;
    }

    public PersonalInformationBean getPersonalInformationBean() {
        return personalInformationBean;
    }
}
